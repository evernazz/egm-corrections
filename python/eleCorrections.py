import os
import envyaml

from analysis_tools.utils import import_root
ROOT = import_root()

import correctionlib
correctionlib.register_pyroot_binding()

corrCfg = envyaml.EnvYAML('%s/src/Corrections/EGM/python/eleCorrectionsFiles.yaml' % 
                                    os.environ['CMSSW_BASE'])

class eleSFRDFProducer():
    def __init__(self, *args, **kwargs):
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year") ; year = str(self.year)
        self.wps = kwargs.pop("wps")

        prefix = "" ; isUL = False
        try:
            isUL = kwargs.pop("isUL")
            prefix += "" if not isUL else "UL"
        except KeyError:
            pass
        try:
            prefix += kwargs.pop("runPeriod")
        except KeyError:
            pass

        self.corrKey = prefix + year
        
        if self.isMC:
            if not isUL and self.year < 2022:
                raise ValueError("Only implemented for Run2 UL datasets")

            if not os.getenv("_corr"):
                os.environ["_corr"] = "_corr"

                if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
                    ROOT.gInterpreter.Load("libBaseModules.so")

                ROOT.gInterpreter.Declare(os.path.expandvars(
                    '#include "$CMSSW_BASE/src/Base/Modules/interface/correctionWrapper.h"'))

            if not os.getenv(f"_eleSF_{self.corrKey}"):
                os.environ[f"_eleSF_{self.corrKey}"] = "_eleSF"
                ROOT.gInterpreter.ProcessLine(
                    'auto corr_%s = MyCorrections("%s", "%s");' %
                        (self.corrKey, corrCfg["SF"][self.corrKey]["fileName"],
                            corrCfg["SF"][self.corrKey]["corrName"]))

                ROOT.gInterpreter.Declare("""
                    using Vfloat = const ROOT::RVec<float>&;
                    using Vint = const ROOT::RVec<int>&;
                    ROOT::RVec<double> get_ele_sf_%s( // version without phi
                            std::string syst, std::string wp, Vfloat eta, Vfloat pt) {
                        ROOT::RVec<double> sf;
                        for (size_t i = 0; i < pt.size(); i++) {
                            if (pt[i] < 20.) sf.push_back(1.); // We are not using electrons of pt <20 GeV as candidate tau_e
                            // (because RecoAbove20 of course does not work for ele pt < 20)
                            else sf.push_back(corr_%s.eval({"%s", syst, wp, eta[i], pt[i]}));
                        }
                        return sf;
                    }
                    ROOT::RVec<double> get_ele_sf_%s( // Version with phi (for 2023 ?)
                            std::string syst, std::string wp, Vfloat eta, Vfloat pt, Vfloat phi) {
                        ROOT::RVec<double> sf;
                        for (size_t i = 0; i < pt.size(); i++) {
                            if (pt[i] < 10.) sf.push_back(1.);
                            else sf.push_back(corr_%s.eval({"%s", syst, wp, eta[i], pt[i], phi[i]}));
                        }
                        return sf;
                    }
                """ % (self.corrKey, self.corrKey, corrCfg["SF"][self.corrKey]["yearkey"],
                       self.corrKey, self.corrKey, corrCfg["SF"][self.corrKey]["yearkey"]))

    def run(self, df):
        if not self.isMC:
            return df, []
        if "Electron_deltaEtaSC" in df.GetColumnNames():
            sc_eta = "Electron_eta+Electron_deltaEtaSC"
        else:
            print("WARNING : Electron_deltaEtaSC branch not available, using electron eta instead of supercluster eta")
            sc_eta = "Electron_eta" # I think normally we should use Electron_eta+Electron_deltaEtaSC but we don't have Electron_deltaEtaSC in the Run2 skims produced by HTT group (should have marginal impact)
        branches = []
        for syst_name, syst in [("", "sf"), ("_up", "sfup"), ("_down", "sfdown")]:
            for wp in self.wps:
                if self.year <= 2022:
                    df = df.Define("elesf_%s%s" % (wp, syst_name),
                        f'get_ele_sf_{self.corrKey}("{syst}", "{wp}", {sc_eta}, Electron_pt)')

                else:
                    df = df.Define("elesf_%s%s" % (wp, syst_name),
                        f'get_ele_sf_{self.corrKey}("{syst}", "{wp}", {sc_eta}, Electron_pt, Electron_phi)' %
                        (self.corrKey, syst, wp))

                branches.append("elesf_%s%s" % (wp, syst_name))

        return df, branches

class eleScaleUncertaintiesRDFProducer():
    """ Computing scale uncertainties for electrons. Instructions are a mess !
    https://cms-talk.web.cern.ch/t/pnoton-energy-corrections-in-nanoaod-v11/34327/2
    https://twiki.cern.ch/twiki/bin/view/CMS/EgammaUL2016To2018#Scale_and_smearing_corrections_f
    https://gitlab.cern.ch/cms-analysis/general/HiggsDNA/-/blob/master/higgs_dna/systematics/photon_systematics.py
    """
    def __init__(self, *args, **kwargs):
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year")
        self.ispreVFP = kwargs.pop("ispreVFP", None)
        self.isUL = kwargs.pop("isUL")

        if self.isMC:
            json_path_scaleUncertainties = os.path.expandvars("$CMSSW_BASE/src/Corrections/EGM/data/ScaleUncertainties/{}/EGM_ScaleUnc.json.gz")
            if not self.isUL:
                raise ValueError("Only implemented for UL datasets")
            if self.year == 2016:
                if self.ispreVFP:   self.name = "2016preVFP_UL";    self.tag = "2016preVFP"
                else:               self.name = "2016postVFP_UL";   self.tag = "2016postVFP"
            elif self.year == 2017: self.name = "2017_UL";          self.tag = "2017"
            elif self.year == 2018: self.name = "2018_UL";          self.tag = "2018"
            filename = json_path_scaleUncertainties.format(self.name)

            if not os.getenv("_corr"):
                os.environ["_corr"] = "_corr"

                if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
                    ROOT.gInterpreter.Load("libBaseModules.so")

                ROOT.gInterpreter.Declare(os.path.expandvars(
                    '#include "$CMSSW_BASE/src/Base/Modules/interface/correctionWrapper.h"'))

            if not os.getenv("_eleScaleUncertainties"):
                os.environ["_eleScaleUncertainties"] = "_eleScaleUncertainties"

                ROOT.gInterpreter.ProcessLine(
                        'auto corr_ele_scaleUncertainty = MyCorrections("%s", "UL-EGM_ScaleUnc");' % filename)
                #  UL-EGM_ScaleUnc (v2)
                #     These are the Scale unc (up or down) for 2018 Ultra Legacy dataset.
                #     Node counts: Category: 5, MultiBinning: 9
                #     ╭────────────────── ▶ input ──────────────────╮ ╭────────────── ▶ input ───────────────╮
                #     │ year (string)                               │ │ ValType (string)                     │
                #     │ year/scenario: example 2016preVFP, 2017 etc │ │ ssup/ssdown                          │
                #     │ Values: 2018                                │ │ Values: scaledown, scaleunc, scaleup │
                #     ╰─────────────────────────────────────────────╯ ╰──────────────────────────────────────╯
                #     ╭────────────────── ▶ input ──────────────────╮ ╭────────────── ▶ input ───────────────╮
                #     │ eta (real)                                  │ │ gain (int)                           │
                #     │ No description                              │ │ No description                       │
                #     │ Range: [-inf, inf)                          │ │ Values: 1, 6, 12                     │
                #     ╰─────────────────────────────────────────────╯ ╰──────────────────────────────────────╯
                #     ╭──────── ◀ output ─────────╮
                #     │ unc (real)                │
                #     │ value of unc (up or down) │
                #     ╰───────────────────────────╯

                # scaleup will be ~1.02, scaledown ~0.98, scaleunc ~0.02
                ROOT.gInterpreter.Declare("""
                        using Vfloat = const ROOT::RVec<float>&;

                        std::pair<ROOT::RVec<float>, ROOT::RVec<float>> eleScaleUncertainties(Vfloat Electron_pt, Vfloat Electron_eta, const ROOT::RVec<UChar_t>& Electron_seedGain) {
                            ROOT::RVec<float> corr_pt_up, corr_pt_down;
                            corr_pt_up.reserve(Electron_pt.size());
                            corr_pt_down.reserve(Electron_pt.size());
                            for (size_t i = 0; i < Electron_eta.size(); i++) {
                                corr_pt_up.push_back(Electron_pt[i] * corr_ele_scaleUncertainty.eval({"%s", "scaleup", Electron_eta[i], Electron_seedGain[i]}));
                                corr_pt_down.push_back(Electron_pt[i] * corr_ele_scaleUncertainty.eval({"%s", "scaledown", Electron_eta[i], Electron_seedGain[i]}));
                            }
                            return {corr_pt_up, corr_pt_down};
                        }
                                          
                        
                        std::pair<ROOT::RVec<float>, ROOT::RVec<float>> eleSmearingUncertainties(Vfloat Electron_pt, Vfloat Electron_eta, Vfloat Electron_phi, Vfloat Electron_mass, Vfloat Electron_dEsigmaUp, Vfloat Electron_dEsigmaDown) {
                            ROOT::RVec<float> corr_pt_up, corr_pt_down;
                            corr_pt_up.reserve(Electron_pt.size());
                            corr_pt_down.reserve(Electron_pt.size());
                            for (size_t i = 0; i < Electron_eta.size(); i++) {
                                float invChEta = 1. / std::cosh(Electron_eta[i]);

                                corr_pt_up.push_back(Electron_pt[i] + std::abs(Electron_dEsigmaUp[i])*invChEta);
                                corr_pt_down.push_back(Electron_pt[i] - std::abs(Electron_dEsigmaDown[i])*invChEta);
                            }
                            return {corr_pt_up, corr_pt_down};
                        }
                    """ % (self.tag, self.tag))
                


    def run(self, df):
        branches = []

        if self.isMC:

            # Energy corrections are applied on electron energy, so convert pt to energy, apply correction, then back to pt
            # not entirely sure this is correct
            df = df.Define("Electron_scale_result", "eleScaleUncertainties(Electron_pt, Electron_eta, Electron_seedGain)")
            df = df.Define("Electron_pt_scale_up", "Electron_scale_result.first")
            df = df.Define("Electron_pt_scale_down", "Electron_scale_result.second")
            # Not sure if we need to smear the mass as well
            df = df.Define("Electron_mass_scale_up", "Electron_mass")
            df = df.Define("Electron_mass_scale_down", "Electron_mass")

            # Electron_dEsigmaUp is centered at 0 and is around ~0.01
            df = df.Define("Electron_smear_result", "eleSmearingUncertainties(Electron_pt, Electron_eta, Electron_phi, Electron_mass, Electron_dEsigmaUp, Electron_dEsigmaDown)")
            df = df.Define("Electron_pt_smear_up", "Electron_smear_result.first")
            df = df.Define("Electron_pt_smear_down", "Electron_smear_result.second")
            # Not sure if we need to smear the mass as well
            df = df.Define("Electron_mass_smear_up", "Electron_mass")
            df = df.Define("Electron_mass_smear_down", "Electron_mass")
            
            branches = ["Electron_pt_smear_up", "Electron_pt_smear_down", "Electron_pt_scale_up", "Electron_pt_scale_down",
                        "Electron_mass_smear_up", "Electron_mass_smear_down", "Electron_mass_scale_up", "Electron_mass_scale_down"]

        return df, branches


class eleSSRDFProducer():
    def __init__(self, *args, **kwargs):
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year") ; year = str(self.year)
        self.prefix = kwargs.pop("runPeriod")
        self.corrKey = self.prefix + year

        if not os.getenv("_corr"):
            os.environ["_corr"] = "_corr"
            if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gInterpreter.Load("libBaseModules.so")
            ROOT.gInterpreter.Declare(os.path.expandvars(
                '#include "$CMSSW_BASE/src/Base/Modules/interface/correctionWrapper.h"'))

        if not self.isMC:
            if self.year != 2022:
                raise ValueError("Only available for Run3 2022 data datasets for now")

            if not os.getenv(f"_eleSS_data_{self.corrKey}"):
                os.environ[f"_eleSS_data_{self.corrKey}"] = "_eleSS"

                ROOT.gInterpreter.ProcessLine(
                    'auto corr_%s = MyCorrections("%s", "%s");' %
                        (self.corrKey, corrCfg["SS"][self.corrKey]["fileName"],
                            corrCfg["SS"][self.corrKey]["corrNameScale"]))

                ROOT.gInterpreter.Declare("""
                    using Vfloat = const ROOT::RVec<float>&;
                    using Vint = const ROOT::RVec<int>&;
                    ROOT::RVec<double> get_ele_ss_%s(
                            Double_t run,
                            Vint gain, Vfloat eta, Vfloat r9, Vfloat pt) {
                        ROOT::RVec<double> sf;
                        for (size_t i = 0; i < pt.size(); i++) {
                            if (pt[i] < 10.) sf.push_back(1.);
                            else {
                                sf.push_back(corr_%s.eval({"total_correction", gain[i], run, eta[i], r9[i], pt[i]}));
                            }
                        }
                        return sf;
                    }
                """ % (self.corrKey, self.corrKey))

        else:
            if self.year != 2022:
                raise ValueError("Only available for Run3 2022 MC datasets for now")

            if not os.getenv(f"_eleSS_mc_{self.corrKey}"):
                os.environ[f"_eleSS_mc_{self.corrKey}"] = "_eleSS"

                ROOT.gInterpreter.ProcessLine(
                    'auto corr_smear_%s = MyCorrections("%s", "%s");' %
                        (self.corrKey, corrCfg["SS"][self.corrKey]["fileName"],
                            corrCfg["SS"][self.corrKey]["corrNameSmear"]))

                ROOT.gInterpreter.ProcessLine(
                    'auto corr_scale_%s = MyCorrections("%s", "%s");' %
                        (self.corrKey, corrCfg["SS"][self.corrKey]["fileName"],
                            corrCfg["SS"][self.corrKey]["corrNameScale"]))

                ROOT.gInterpreter.Declare("""
                    using Vfloat = const ROOT::RVec<float>&;
                    using Vint = const ROOT::RVec<int>&;
                    TRandom rndm_%s(7);
                    ROOT::RVec<double> get_ele_ss_%s(
                            std::string syst, Vfloat eta, Vfloat r9, Vfloat pt) {
                        ROOT::RVec<double> sf;
                        for (size_t i = 0; i < pt.size(); i++) {
                            if (pt[i] < 10.) sf.push_back(1.);
                            else {
                                if (syst == "down") {
                                    Double_t smearing = corr_smear_%s.eval({"rho", eta[i], r9[i]});
                                    Double_t uncrtnty = corr_smear_%s.eval({"err_rho", eta[i], r9[i]});
                                    sf.push_back(rndm_%s.Gaus(1., smearing - uncrtnty));
                                }
                                else if (syst == "up") {
                                    Double_t smearing = corr_smear_%s.eval({"rho", eta[i], r9[i]});
                                    Double_t uncrtnty = corr_smear_%s.eval({"err_rho", eta[i], r9[i]});
                                    sf.push_back(rndm_%s.Gaus(1., smearing + uncrtnty));
                                }
                                else {
                                    Double_t smearing = corr_smear_%s.eval({"rho", eta[i], r9[i]});
                                    sf.push_back(rndm_%s.Gaus(1., smearing));
                                }
                            }
                        }
                        return sf;
                    }

                    ROOT::RVec<double> get_ele_scale_unc_%s(
                            std::string syst, Double_t run,
                            Vint gain, Vfloat eta, Vfloat r9, Vfloat pt) {
                        ROOT::RVec<double> sf;
                        for (size_t i = 0; i < pt.size(); i++) {
                            if (pt[i] < 10.) sf.push_back(1.);
                            else {
                                if (syst == "down") {
                                    sf.push_back(1. - corr_scale_%s.eval({"total_uncertainty", gain[i], run, eta[i], r9[i], pt[i]}));
                                }
                                else {
                                    sf.push_back(1. + corr_scale_%s.eval({"total_uncertainty", gain[i], run, eta[i], r9[i], pt[i]}));
                                }
                            }
                        }
                        return sf;
                    }
                """ % (self.corrKey, self.corrKey, self.corrKey, self.corrKey, self.corrKey,
                    self.corrKey, self.corrKey, self.corrKey, self.corrKey, self.corrKey,
                    self.corrKey, self.corrKey, self.corrKey))


    def run(self, df):
        branches = []
        if not self.isMC:
            df = df.Define("eless_scale",
                            'get_ele_ss_%s(run, Electron_seedGain, Electron_eta, Electron_r9, Electron_pt)'
                            % self.corrKey)

            df = df.Define("Electron_pt_corr", "Electron_pt * eless_scale")
            df = df.Define("Electron_mass_corr", "Electron_mass * eless_scale")

            branches.append("eless_scale")
            branches.append("Electron_pt_corr")
            branches.append("Electron_mass_corr")

        else:
            for syst_name, syst in [("", "nom"), ("_up", "up"), ("_down", "down")]:
                df = df.Define("eless_smear%s" % syst_name,
                                'get_ele_ss_%s("%s", Electron_eta, Electron_r9, Electron_pt)'
                                % (self.corrKey, syst))

                df = df.Define("Electron_pt_corr%s" % syst_name,
                                "Electron_pt * eless_smear%s" % syst_name)

                df = df.Define("Electron_mass_corr%s" % syst_name,
                                "Electron_mass * eless_smear%s" % syst_name)

                branches.append("eless_smear%s" % syst_name)
                branches.append("Electron_pt_corr%s" % syst_name)
                branches.append("Electron_mass_corr%s" % syst_name)

                # account for the systematic arising from the scale correction on data
                if syst != "nom":
                    df = df.Define("eless_scale%s" % syst_name,
                            'get_ele_scale_unc_%s("%s", run, Electron_seedGain, Electron_eta, Electron_r9, Electron_pt)'
                            % (self.corrKey, syst))

                    df = df.Define("Electron_pt_corr_scale%s" % syst_name,
                                    "Electron_pt_corr * eless_scale%s" % syst_name)

                    df = df.Define("Electron_mass_corr_scale%s" % syst_name,
                                    "Electron_mass_corr * eless_scale%s" % syst_name)

                    branches.append("eless_scale%s" % syst_name)
                    branches.append("Electron_pt_corr_scale%s" % syst_name)
                    branches.append("Electron_mass_corr_scale%s" % syst_name)

        return df, branches


def eleSFRDF(**kwargs):
    """
    Module to obtain electron SFs with their uncertainties.

    :param wps: name of the wps to consider among ``Loose``, ``Medium``,
        ``RecoAbove20``, ``RecoBelow20``, ``Tight``, ``Veto``, ``wp80iso`` (default),
        ``wp80noiso``, ``wp90iso,`` ``wp90noiso``.
    :type wps: list of str

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: eleSFRDF
            path: Corrections.EGM.eleCorrections
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                runPeriod: self.config.get_aux('runPeriod', False)
                isUL: self.dataset.has_tag('ul')
                wps: [wp80iso, ...]
    """

    return lambda: eleSFRDFProducer(**kwargs)

def eleSSRDF(**kwargs):
    """
    Module to obtain electron SFs with their uncertainties.

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: eleSSRDF
            path: Corrections.EGM.eleCorrections
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                runPeriod: self.config.get_aux('runPeriod', False)
                isUL: self.dataset.has_tag('ul')
    """

    return lambda: eleSSRDFProducer(**kwargs)

def eleScaleUncertaintiesRDF(**kwargs):
    """
    Module to obtain scale uncertainties for electrons. Computing Electron_pt_smear_up/down & Electron_pt_scale_up/down for MC
    Needs a clone of https://github.com/cms-egamma/ScaleFactorsJSON.git inside Corrections/EGM/data/ScaleUncertainties
    (usually done in setup.sh)

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: eleSFRDF
            path: Corrections.EGM.eleCorrections
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                isUL: self.dataset.has_tag('ul')
                ispreVFP: self.config.get_aux('ispreVFP', False)
    """

    return lambda: eleScaleUncertaintiesRDFProducer(**kwargs)